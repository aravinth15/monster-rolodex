import React from 'react';
import Card from './Card/Card';
import './CardList.css';

function CardList(props) {
    return (
        <div className='card-list'>
            {props.monsters.map(user => <Card key={user.id} monster = {user}/>)}
        </div>
    )
}

export default CardList
