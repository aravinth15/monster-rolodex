import React,{useState,useEffect} from 'react';
import CardList from './CardList/CardList';
import {SearchBox} from './SearchBox/SearchBox';
import './App.css';

function App() {

  const [monsters,setMonsters] = useState([])
  const [search,setSearch] = useState('')

  useEffect(() => fetch('https://jsonplaceholder.typicode.com/users').then(response => response.json()).then(users => setMonsters(users)), []);
  
  const filteredMonster = monsters.filter(monster => monster.name.toLowerCase().includes(search.toLowerCase()))

  return (
    <div className="App">
      <h1>Monster Rolodex</h1>
      <SearchBox handleChange={e => setSearch(e.target.value)}/>
      <CardList monsters = {filteredMonster} />
    </div>
  );
}

export default App;
